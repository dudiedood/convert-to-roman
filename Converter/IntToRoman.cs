﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanConverter
{
    public class IntToRoman
    {
        string roman = string.Empty;

        int [] intCheckpoints = new int [] {1000,900,500,400,100,90,50,40,10,9,5,4,3,2,1};
        string[] romanTranslation = new string[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "III", "II", "I" };
        
        public string toRoman(int p)
        {
            if (p > 3999 || p < 1) return "NARN";
            
            else 
            {
                while (p > 0) {
                    for (var i = 0; i < intCheckpoints.Length; i++)
                    {
                        if (p >= intCheckpoints[i])
                        {
                            p = p - intCheckpoints[i];
                            roman += romanTranslation[i];
                            break;
                        }

                    }
                }
                
                //if (p != 0) this.toRoman(p);
                return roman;
            };

            //else if (p >= 1000)
            //{
            //    p = p - 1000;
            //    roman += "M";
            //}

            //else if (p >= 900)
            //{
            //    p = p - 900;
            //    roman += "CM";
            //}

            //else if (p >= 500)
            //{
            //    p = p - 500;
            //    roman += "D";
            //}

            //else if (p >= 400)
            //{
            //    p = p - 400;
            //    roman += "CD";
            //}

            //else if (p >= 100)
            //{
            //    p = p - 100;
            //    roman += "C";
            //}

            //else if (p >= 90)
            //{
            //    p = p - 90;
            //    roman += "XC";
            //}

            //else if (p >= 50)
            //{
            //    p = p - 50;
            //    roman += "L";
            //}

            //else if (p >= 40)
            //{
            //    p = p - 40;
            //    roman += "XL";
            //}

            //else if (p >= 10)
            //{                
            //    p = p - 10;
            //    roman +=  "X";
            //}

            //else if (p >= 9)
            //{
            //    p = p - 9;
            //    roman += "IX";
            //}

            //else if (p >= 5)
            //{
            //    p = p - 5;
            //    roman += "V" ;
            //}


            //else if (p >= 4)
            //{
            //    p = p - 4;
            //    roman += "IV" ;
            //}

            //else if (p == 3)
            //{
            //    p = p - 3;
            //    roman += "III" ;
            //}

            //else if (p == 2)
            //{
            //    p = p - 2;
            //    roman += "II" ;
            //}

            //else if (p == 1)
            //{
            //    p = p - 1;
            //    roman += "I" ;
            //}
            

        }

    }

}

