﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RomanConverter;


namespace ConsoleConvert
{
    class Program
    {
        

        static void Main(string[] args)
        {
            var toConvert = new IntToRoman();
            Console.Write("Enter an integer between 1 and 3999 :");
            var arg1 = Console.ReadLine();
            var roman = toConvert.toRoman(int.Parse(arg1));
            Console.WriteLine("... as roman numerals is {0}",roman);
            Console.ReadLine();
        }
    }
}
