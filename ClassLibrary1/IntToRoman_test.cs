﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RomanConverterTest;
using NUnit.Framework;
using RomanConverter;

namespace RomanConverterTest
{   
    [TestFixture]
    public class IntToRoman_test
    {
        
        [TestCase("I",1)]
        [TestCase("II",2)]
        [TestCase("III",3)]
        [TestCase("IV",4)]
        [TestCase("V",5)]
        [TestCase("X",10)]
        [TestCase("XIII", 13)]
        [TestCase("LIII", 53)]
        [TestCase("XCIV", 94)]
        [TestCase("CCXLIV", 244)]
        [TestCase("CCLXXVII", 277)]
        [TestCase("CDXXVIII", 428)]
        [TestCase("DLXXXIX", 589)]
        [TestCase("CMLXI", 961)]
        [TestCase("MMDLXXXIX", 2589)]

        public void that_number_returns_a_roman_test(string roman,int value)
        {
            IntToRoman converter = new IntToRoman();
            var check = converter.toRoman(value);
            Assert.AreEqual(roman, check);
        }

        [TestCase("NARN", 4000)]
        [TestCase("NARN", -1)]
        public void that_number_returns_NARN(string roman, int value)
        {
            IntToRoman converter = new IntToRoman();
            var check = converter.toRoman(value);
            Assert.AreEqual(roman, check);
        }
        

    }

   
}
